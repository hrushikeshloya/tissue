//
// Filename     : growthForce.cc
// Description  : Classes describing growth updates by applying forces on vertices
// Author(s)    : Henrik Jonsson (henrik.jonsson@slcu.cam.ac.uk)
// Created      : April 2018
// Revision     : $Id:$
//

#include "growthForce.h"
#include <utility>
#include <vector>
#include "baseReaction.h"
#include "tissue.h"
#include "myTimes.h"

namespace GrowthForce {
  Radial::
  Radial(std::vector<double> &paraValue, 
	 std::vector< std::vector<size_t> > 
	 &indValue ) {
    
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=2 || ( paraValue[1]!=0 && paraValue[1]!=1) ) {
      std::cerr << "GrowthForce::Radial::"
		<< "Radial() "
		<< "Uses two parameters k_growth and r_pow (0,1)" << std::endl;
      exit(EXIT_FAILURE);
    }  
    if( indValue.size() != 0 ) {
      std::cerr << "GrowthForce::Radial::"
		<< "Radial() "
		<< "No variable index is used." << std::endl;
      exit(EXIT_FAILURE);
    }
    // Set the variable values
    //
    setId("Force::Radial");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "k_growth";
    tmp[0] = "r_pow";
    setParameterId( tmp );
  }

  void Radial::
  derivs(Tissue &T,
	 DataMatrix &cellData,
	 DataMatrix &wallData,
	 DataMatrix &vertexData,
	 DataMatrix &cellDerivs,
	 DataMatrix &wallDerivs,
	 DataMatrix &vertexDerivs ) {
    
    size_t numVertices = T.numVertex();
    size_t dimension=vertexData[0].size();
    
    for( size_t i=0 ; i<numVertices ; ++i ) {
      double fac=parameter(0);
      if( parameter(1)==0.0 ) {
	double r=0.0;
	for( size_t d=0 ; d<dimension ; ++d )
	  r += vertexData[i][d]*vertexData[i][d];
	if( r>0.0 )
	  r = std::sqrt(r);
	if( r>0.0 )
	  fac /= r;
	else
	  fac=0.0;
      }
      for( size_t d=0 ; d<dimension ; ++d )
	vertexDerivs[i][d] += fac*vertexData[i][d];
    }
  }
  
  void Radial::derivsWithAbs(Tissue &T,
			     DataMatrix &cellData,
			     DataMatrix &wallData,
			     DataMatrix &vertexData,
			     DataMatrix &cellDerivs,
			     DataMatrix &wallDerivs,
			     DataMatrix &vertexDerivs,
			     DataMatrix &sdydtCell,
			     DataMatrix &sdydtWall,
			     DataMatrix &sdydtVertex )  {
    return derivs(T,cellData,wallData,vertexData,cellDerivs,wallDerivs,vertexDerivs);
  }

  namespace CenterTriangulation {
    Radial::
    Radial(std::vector<double> &paraValue, 
	   std::vector< std::vector<size_t> > 
	   &indValue ) {      
      // Do some checks on the parameters and variable indeces
      //
      if( paraValue.size()!=2 || ( paraValue[1]!=0 && paraValue[1]!=1) ) {
	std::cerr << "GrowthForce::CenterTriangulation::Radial::"
		  << "Radial() " << std::endl
		  << "Uses two parameters k_growth and r_pow (0,1)" << std::endl;
	exit(EXIT_FAILURE);
      }  
      if( indValue.size() != 1 || indValue[0].size() != 1 ) {
	std::cerr << "GrowthForce::CenterTriangulation::Radial::"
		  << "Radial() " << std::endl
		  << "Start of additional Cell variable indices (center(x,y,z) "
		  << "L_1,...,L_n, n=num vertex) is given in first level. "
		  << "See Documentation for namespace CenterTriangulation."
		  << std::endl;
	exit(EXIT_FAILURE);
      }
      // Set the variable values
      //
      setId("GrowthForce::CenterTriangulation::Radial");
      setParameter(paraValue);  
      setVariableIndex(indValue);
      
      // Set the parameter identities
      //
      std::vector<std::string> tmp( numParameter() );
      tmp.resize( numParameter() );
      tmp[0] = "k_growth";
      tmp[0] = "r_pow";
      setParameterId( tmp );
    }

    void Radial::
    derivs(Tissue &T,
	   DataMatrix &cellData,
	   DataMatrix &wallData,
	   DataMatrix &vertexData,
	   DataMatrix &cellDerivs,
	   DataMatrix &wallDerivs,
	   DataMatrix &vertexDerivs ) {
      
      size_t numVertices = T.numVertex();
      size_t numCells = T.numCell();
      size_t dimension=vertexData[0].size();
      
      // Move vertices / apply force to vertices
      for( size_t i=0 ; i<numVertices ; ++i ) {
	double fac=parameter(0);
	if( parameter(1)==0.0 ) {
	  double r=0.0;
	  for( size_t d=0 ; d<dimension ; ++d )
	    r += vertexData[i][d]*vertexData[i][d];
	  if( r>0.0 )
	    r = std::sqrt(r);
	  if( r>0.0 )
	    fac /= r;
	  else
	    fac=0.0;
	}
	for( size_t d=0 ; d<dimension ; ++d )
	  vertexDerivs[i][d] += fac*vertexData[i][d];
      }
      // Move / apply force to vertices defined in cell centers 
      for( size_t i=0 ; i<numCells ; ++i ) {
	double fac=parameter(0);
	if( parameter(1)==0.0 ) {
	  double r=0.0;
	  for( size_t d=variableIndex(0,0) ; d<variableIndex(0,0)+dimension ; ++d )
	    r += cellData[i][d]*cellData[i][d];
	  if( r>0.0 )
	    r = std::sqrt(r);
	  if( r>0.0 )
	    fac /= r;
	  else
	    fac=0.0;
	}
	for( size_t d=variableIndex(0,0) ; d<variableIndex(0,0)+dimension ; ++d )
	  cellDerivs[i][d] += fac*cellData[i][d];
      }
    }
  } // end namespace CenterTriangulation

  EpidermalRadial::
  EpidermalRadial(std::vector<double> &paraValue, 
		  std::vector< std::vector<size_t> > 
		  &indValue ) {
    
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=2 || ( paraValue[1]!=0 && paraValue[1]!=1) ) {
      std::cerr << "GrowthForce::EpidermalRadial::"
		<< "EpidermalRadial() "
		<< "Uses two parameters k_growth and r_pow (0,1)"
		<< std::endl;
      exit(EXIT_FAILURE);
    }  
    if( indValue.size() != 0 ) {
      std::cerr << "GrowthForce::EpidermalRadial::"
		<< "EpidermalRadial() "
		<< "No variable index is used."
		<< std::endl;
      exit(EXIT_FAILURE);
    }
    // Set the variable values
    //
    setId("GrowthForce::EpidermalRadial");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "k_growth";
    tmp[0] = "r_pow";
    setParameterId( tmp );
  }

  void EpidermalRadial::
  derivs(Tissue &T,
	 DataMatrix &cellData,
	 DataMatrix &wallData,
	 DataMatrix &vertexData,
	 DataMatrix &cellDerivs,
	 DataMatrix &wallDerivs,
	 DataMatrix &vertexDerivs ) {
    
    size_t numVertices = T.numVertex();
    size_t dimension=vertexData[0].size();

    for( size_t i=0 ; i<numVertices ; ++i ) {
      if( T.vertex(i).isBoundary(T.background())) {
	double fac=parameter(0);
	if( parameter(1)==0.0 ) {
	  double r=0.0;	    
	  for( size_t d=0 ; d<dimension ; ++d )
	    r += vertexData[i][d]*vertexData[i][d];
	  if( r>0.0 )
	    r = std::sqrt(r);
	  if( r>0.0 )
	    fac /= r;
	  else
	    fac=0.0;
	}
	for( size_t d=0 ; d<dimension ; ++d )
	  vertexDerivs[i][d] += fac*vertexData[i][d];	  
      }      
    }
  }
  
  X::
  X(std::vector<double> &paraValue, 
    std::vector< std::vector<size_t> > 
    &indValue ) {
    
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=2 || ( paraValue[1]!=0 && paraValue[1]!=1) ) {
      std::cerr << "GrowthForce::X::"
		<< "X() "
		<< "Uses two parameters k_growth and growth_mode (0,1)"
		<< std::endl;
      exit(EXIT_FAILURE);
    }  
    if( indValue.size() != 0 ) {
      std::cerr << "GrowthForce::X::"
		<< "X() "
		<< "No variable index is used." << std::endl;
      exit(EXIT_FAILURE);
    }
    // Set the variable values
    //
    setId("GrowthForce::X");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "k_growth";
    tmp[1] = "growth_mode";
    setParameterId( tmp );
  }

  void X::
  derivs(Tissue &T,
	 DataMatrix &cellData,
	 DataMatrix &wallData,
	 DataMatrix &vertexData,
	 DataMatrix &cellDerivs,
	 DataMatrix &wallDerivs,
	 DataMatrix &vertexDerivs ) {
    
    size_t numVertices = T.numVertex();
    size_t s_i = 0; // spatial index
    double fac=parameter(0);
    //size_t dimension=vertexData[s_i].size();
    size_t growth_mode = parameter(1);
    
    for( size_t i=0 ; i<numVertices ; ++i ) {
      if( growth_mode == 1 ) {
	vertexDerivs[i][s_i] += fac*vertexData[i][s_i];
      }
      else {
	if( vertexData[i][s_i] >=0 ) {
	  vertexDerivs[i][s_i] += fac;
	}
	else {
	  vertexDerivs[i][s_i] -= fac;
	}
      }
    }
  }
  
  void X::
  derivsWithAbs(Tissue &T,
		DataMatrix &cellData,
		DataMatrix &wallData,
		DataMatrix &vertexData,
		DataMatrix &cellDerivs,
		DataMatrix &wallDerivs,
		DataMatrix &vertexDerivs,
		DataMatrix &sdydtCell,
		DataMatrix &sdydtWall,
		DataMatrix &sdydtVertex ){
    return derivs(T,cellData,wallData,vertexData,cellDerivs,wallDerivs,vertexDerivs);
  }
  

  XGeneral::
  XGeneral(std::vector<double> &paraValue, 
    std::vector< std::vector<size_t> > 
    &indValue ) {
    
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=3 ) {
      std::cerr << "GrowthForce::XGeneral::"
    << "XGeneral() "
    << "Uses three parameters target_for_growth, Centroid_index, and Normalization"
    << std::endl;
      exit(EXIT_FAILURE);
    }  
    if( indValue.size() != 0 ) {
      std::cerr << "GrowthForce::X::"
    << "X() "
    << "No variable index is used." << std::endl;
      exit(EXIT_FAILURE);
    }
    // Set the variable values
    //
    setId("GrowthForce::X");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "target_for_growth";
    tmp[1] = "centroid_index";
    tmp[2] = "growth_mode";
    setParameterId( tmp );
  }

  void XGeneral::
  derivs(Tissue &T,
   DataMatrix &cellData,
   DataMatrix &wallData,
   DataMatrix &vertexData,
   DataMatrix &cellDerivs,
   DataMatrix &wallDerivs,
   DataMatrix &vertexDerivs ) {
    
    size_t numVertices = T.numVertex();
    size_t s_i = 0; // spatial index
    size_t target = parameter(0);
    size_t centroid_index = parameter(1);
    double normalization = parameter(2);
    double growthRate;

for( size_t i=0 ; i<numVertices ; ++i ) {
  double nearest = 100;
  int cell_id = 0;
  growthRate = 0;

// This for loop calculates the nearest cell to the cell vertex
for (size_t k = 0; k<T.numCell(); k++){
  if(vertexData[i][s_i] > cellData[k][centroid_index]){
  if((fabs((double)(vertexData[i][s_i] - cellData[k][centroid_index])) < nearest)){
    nearest = (double) fabs(vertexData[i][s_i] - cellData[k][centroid_index]);
    cell_id = k;
  }
}

}
  for (size_t j=0; j<numVertices; ++j){ 
  if(vertexData[j][s_i] + 0.1 >= vertexData[i][s_i]){
    if(nearest == 100){
      // This happens if it is the first cell
      // In that case, recalulate the nearest Centroid 
      for (size_t k = 0; k<T.numCell(); k++){
        if((fabs((double)(vertexData[i][s_i] - cellData[k][centroid_index])) < nearest)){
        nearest = (double) fabs(vertexData[i][s_i] - cellData[k][centroid_index]);
        cell_id = k;
      }
    }
      vertexDerivs[j][s_i] += normalization*nearest*cellData[cell_id][target];
    }
    else{
    vertexDerivs[j][s_i] += normalization*nearest*cellData[cell_id][target];
  }
  }
}
}
}
 

  void XGeneral::
  derivsWithAbs(Tissue &T,
    DataMatrix &cellData,
    DataMatrix &wallData,
    DataMatrix &vertexData,
    DataMatrix &cellDerivs,
    DataMatrix &wallDerivs,
    DataMatrix &vertexDerivs,
    DataMatrix &sdydtCell,
    DataMatrix &sdydtWall,
    DataMatrix &sdydtVertex ){
    return derivs(T,cellData,wallData,vertexData,cellDerivs,wallDerivs,vertexDerivs);
  }
  


  Y::
  Y(std::vector<double> &paraValue, 
    std::vector< std::vector<size_t> > 
    &indValue ) {  
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=2 || ( paraValue[1]!=0 && paraValue[1]!=1) ) {
      std::cerr << "GrowthForce::Y::"
		<< "Y() "
		<< "Uses two parameters k_growth and growth_mode (=0/1)"
		<< std::endl;
      exit(EXIT_FAILURE);
    }  
    if( indValue.size() != 0 ) {
      std::cerr << "GrowthForce::Y::"
		<< "Y() "
		<< "No variable index is used." << std::endl;
      exit(EXIT_FAILURE);
    }
    // Set the variable values
    //
    setId("GrowthForce::Y");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "k_growth";
    tmp[1] = "growth_mode";
    setParameterId( tmp );
  }

  void Y::
  derivs(Tissue &T,
	 DataMatrix &cellData,
	 DataMatrix &wallData,
	 DataMatrix &vertexData,
	 DataMatrix &cellDerivs,
	 DataMatrix &wallDerivs,
	 DataMatrix &vertexDerivs ) {
    
    size_t numVertices = T.numVertex();
    size_t s_i = 1; // spatial index
    //size_t dimension=vertexData[s_i].size();
    double fac=parameter(0);
    size_t growth_mode = parameter(1);
    //std::cout <<  "fac = " << fac << "\n";
    
    for( size_t i=0 ; i<numVertices ; ++i ) {
      if( growth_mode == 1 ) {
	vertexDerivs[i][s_i] += fac*vertexData[i][s_i];
      }
      else {
	if( vertexData[i][s_i] >=0 ) {
	  vertexDerivs[i][s_i] += fac;
	}
	else {
	  vertexDerivs[i][s_i] -= fac;
	}
      }
    }
  }

  void Y::
  derivsWithAbs(Tissue &T,
		DataMatrix &cellData,
		DataMatrix &wallData,
		DataMatrix &vertexData,
		DataMatrix &cellDerivs,
		DataMatrix &wallDerivs,
		DataMatrix &vertexDerivs,
		DataMatrix &sdydtCell,
		DataMatrix &sdydtWall,
		DataMatrix &sdydtVertex ) {
    return derivs(T,cellData,wallData,vertexData,cellDerivs,wallDerivs,vertexDerivs);
  }

YPositive::
  YPositive(std::vector<double> &paraValue, 
    std::vector< std::vector<size_t> > 
    &indValue ) {  
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=2 || ( paraValue[1]!=0 && paraValue[1]!=1) ) {
      std::cerr << "GrowthForce::Y::"
		<< "Y() "
		<< "Uses two parameters k_growth and growth_mode (=0/1)"
		<< std::endl;
      exit(EXIT_FAILURE);
    }  
    if( indValue.size() != 0 ) {
      std::cerr << "GrowthForce::Y::"
		<< "Y() "
		<< "No variable index is used." << std::endl;
      exit(EXIT_FAILURE);
    }
    // Set the variable values
    //
    setId("GrowthForce::Y");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "k_growth";
    tmp[1] = "growth_mode";
    setParameterId( tmp );
  }

  void YPositive::
  derivs(Tissue &T,
	 DataMatrix &cellData,
	 DataMatrix &wallData,
	 DataMatrix &vertexData,
	 DataMatrix &cellDerivs,
	 DataMatrix &wallDerivs,
	 DataMatrix &vertexDerivs ) {
    
    size_t numVertices = T.numVertex();
    size_t s_i = 1; // spatial index
    //size_t dimension=vertexData[s_i].size();
    double fac=parameter(0);
    size_t growth_mode = parameter(1);
    //std::cout <<  "fac = " << fac << "\n";
    
    for( size_t i=0 ; i<numVertices ; ++i ) {
      if( growth_mode == 1 ) {
        if(vertexData[i][s_i] >= 0){
	vertexDerivs[i][s_i] += fac*vertexData[i][s_i];}
    else{
      vertexDerivs[i][s_i] += 0*vertexData[i][s_i];
    }
      }
      else {
	if( vertexData[i][s_i] >=0 ) {
	  vertexDerivs[i][s_i] += fac;
	}
	else {
	  vertexDerivs[i][s_i] -= 0;
	}
      }
    }
  }

  void YPositive::
  derivsWithAbs(Tissue &T,
		DataMatrix &cellData,
		DataMatrix &wallData,
		DataMatrix &vertexData,
		DataMatrix &cellDerivs,
		DataMatrix &wallDerivs,
		DataMatrix &vertexDerivs,
		DataMatrix &sdydtCell,
		DataMatrix &sdydtWall,
		DataMatrix &sdydtVertex ) {
    return derivs(T,cellData,wallData,vertexData,cellDerivs,wallDerivs,vertexDerivs);
  }
  

YNegative::
  YNegative(std::vector<double> &paraValue, 
    std::vector< std::vector<size_t> > 
    &indValue ) {  
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=2 || ( paraValue[1]!=0 && paraValue[1]!=1) ) {
      std::cerr << "GrowthForce::Y::"
    << "Y() "
    << "Uses two parameters k_growth and growth_mode (=0/1)"
    << std::endl;
      exit(EXIT_FAILURE);
    }  
    if( indValue.size() != 0 ) {
      std::cerr << "GrowthForce::Y::"
    << "Y() "
    << "No variable index is used." << std::endl;
      exit(EXIT_FAILURE);
    }
    // Set the variable values
    //
    setId("GrowthForce::Y");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "k_growth";
    tmp[1] = "growth_mode";
    setParameterId( tmp );
  }

  void YNegative::
  derivs(Tissue &T,
   DataMatrix &cellData,
   DataMatrix &wallData,
   DataMatrix &vertexData,
   DataMatrix &cellDerivs,
   DataMatrix &wallDerivs,
   DataMatrix &vertexDerivs ) {
    
    size_t numVertices = T.numVertex();
    size_t s_i = 1; // spatial index
    //size_t dimension=vertexData[s_i].size();
    double fac=parameter(0);
    size_t growth_mode = parameter(1);
    //std::cout <<  "fac = " << fac << "\n";
    
    for( size_t i=0 ; i<numVertices ; ++i ) {
      if( growth_mode == 1 ) {
        if(vertexData[i][s_i] <= 0){
  vertexDerivs[i][s_i] += -fac*vertexData[i][s_i];}
    else{
      vertexDerivs[i][s_i] += 0*vertexData[i][s_i];
    }
      }
      else {
  if( vertexData[i][s_i] >=0 ) {
    vertexDerivs[i][s_i] += 0;
  }
  else {
    vertexDerivs[i][s_i] += fac;
  }
      }
    }
  }

  void YNegative::
  derivsWithAbs(Tissue &T,
    DataMatrix &cellData,
    DataMatrix &wallData,
    DataMatrix &vertexData,
    DataMatrix &cellDerivs,
    DataMatrix &wallDerivs,
    DataMatrix &vertexDerivs,
    DataMatrix &sdydtCell,
    DataMatrix &sdydtWall,
    DataMatrix &sdydtVertex ) {
    return derivs(T,cellData,wallData,vertexData,cellDerivs,wallDerivs,vertexDerivs);
  }
  

  SphereCylinder::
  SphereCylinder(std::vector<double> &paraValue, 
		 std::vector< std::vector<size_t> > 
		 &indValue ) {
    
    // Do some checks on the parameters and variable indeces
    //
    if( paraValue.size()!=2 || ( paraValue[1]!=0 && paraValue[1]!=1) ) {
      std::cerr << "GrowthForce::SphereCylinder::"
		<< "SphereCylinder() "
		<< "Uses two parameters k_growth and r_pow (0,1)"
		<< std::endl;
      exit(EXIT_FAILURE);
    }  
    if( indValue.size() != 0 ) {
      std::cerr << "GrowthForce::SphereCylinder::"
		<< "SphereCylinder() "
		<< "No variable index is used." <<std::endl;
      exit(EXIT_FAILURE);
    }
    // Set the variable values
    //
    setId("GrowthForce::SphereCylinder");
    setParameter(paraValue);  
    setVariableIndex(indValue);
    
    // Set the parameter identities
    //
    std::vector<std::string> tmp( numParameter() );
    tmp.resize( numParameter() );
    tmp[0] = "k_growth";
    tmp[0] = "r_pow";
    setParameterId( tmp );
  }

  void SphereCylinder::
  derivs(Tissue &T,
	 DataMatrix &cellData,
	 DataMatrix &wallData,
	 DataMatrix &vertexData,
	 DataMatrix &cellDerivs,
	 DataMatrix &wallDerivs,
	 DataMatrix &vertexDerivs ) {
    
    size_t numVertices = T.numVertex();
    if (vertexData[0].size()!=3) {
      std::cerr << "GrowthForceSphereCylinder::derivs() "
		<< "Only works for 3 dimensions." << std::endl;
      exit(EXIT_FAILURE);
    }
    size_t xI=0;
    size_t yI=1;
    size_t zI=2;
 
    for( size_t i=0 ; i<numVertices ; ++i ) {
      if (vertexData[i][zI]<0.0) { // on cylinder
	if( parameter(1)==0.0 ) {
	  vertexDerivs[i][zI] -= parameter(0);
	}
	else {
	  double r = std::sqrt(vertexData[i][xI]*vertexData[i][xI]+
			       vertexData[i][yI]*vertexData[i][yI]);
	  vertexDerivs[i][zI] -= parameter(0)*(3.14159265*0.5*r-vertexData[i][zI]);
	}
      }
      else { // on half sphere
	double r = std::sqrt(vertexData[i][xI]*vertexData[i][xI]+
			     vertexData[i][yI]*vertexData[i][yI]+
			     vertexData[i][zI]*vertexData[i][zI]);
	double rPrime = std::sqrt(vertexData[i][xI]*vertexData[i][xI]+
				  vertexData[i][yI]*vertexData[i][yI]);
	double theta = std::asin(rPrime/r);
	
	double fac=parameter(0)*theta;
	if (parameter(0)==1) {
	  fac *= r;
	}
	vertexDerivs[i][xI] += fac*vertexData[i][xI]*vertexData[i][zI]/rPrime;
	vertexDerivs[i][yI] += fac*vertexData[i][yI]*vertexData[i][zI]/rPrime;
	vertexDerivs[i][zI] -= fac*rPrime;
      }
    }
  }
} // end namespace GrowthForce
